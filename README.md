<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## Proceso de configuracion

- Requieres composer link: https://getcomposer.org/
- Servidor local link(opcional): https://laragon.org/download/index.html 
- Comandos a ejecutar para clonacion seguir este tutorial:
https://diarioprogramador.com/como-clonar-un-proyecto-en-laravel-guia-definitiva/
- En el archivo .env se realiza la configuracion de conexion abase de datos y otros servicios. (como MYSQL, Redis, Mail, , AWS, Pusher etc).

- Los controladores se establece en App\Http\Controllers
- Se establece sus funciones con codigo Php.
- Los modelos se encuentra en App\Models\ se establece con la estructura para la identificacion de la tabla que contegas en tu BD.
- Las rutas se establece en routes/web.php (si son rutas para destino a web), si fuera el caso del consumo de apis, se establecen en api.php

- Para estructurar la base de datos con datos de prueba, se crean sus migracciones, seeders, factorys dentro la carpeta databases.

-en public se guardan los archivos css, js, img, etc.

- si llegaras a utiliza puglins de javascript, requieres Node JS
